import React from "react";
import SignIn from "../components/SignIn/SignIn";
import SignUp from "../components/SignUp/SignUp";

const Login = () => {
  const [onSignIn, setOnSignIn] = React.useState(true);

  const handleChangeState = () => {
    setOnSignIn((state) => !state);
  };

  return (
    <>
      {onSignIn ? (
        <SignIn switchToSignUp={handleChangeState} />
      ) : (
        <SignUp switchToSignIn={handleChangeState} />
      )}
    </>
  );
};

export default Login;
