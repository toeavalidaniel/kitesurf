import React from "react";
import axios from "axios";
import AppBar from "../components/AppBar/AppBar";
import Map from "../components/Map/Map";
import Popup from "../components/Popup/Popup";
import FilterButton from "../components/Filter/FilterButton";
import ErrorCard from "../components/ErrorCard/ErrorCard";
import NewSpotModal from "../components/NewSpotModal/NewSpotModal";
import Table from "../components/Table/Table";
import ScrollButton from "../components/ScrollButton/ScrollButton";
import Footer from "../components/Footer/Footer";

const getData = async () => {
  let spots;
  await axios.get("/spot").then((response) => (spots = [...response.data]));

  await axios.get("/favourites").then((response) => {
    let favourites = response.data;
    for (let s of spots) {
      s.favourite = false;
      s.favID = -1;
      for (let f of favourites) {
        if (parseInt(s.id) === parseInt(f.spot)) {
          s.favourite = true;
          s.favID = f.id;
          break;
        }
      }
    }
  });

  return spots;
};

const Dashboard = React.memo(() => {
  const [spots, setSpots] = React.useState({
    normal: [],
    filtered: [],
  });
  const [selectedSpot, setSelectedSpot] = React.useState({});
  const [error, setError] = React.useState(false);
  const [openNewSpotModal, setOpenNewSpotModal] = React.useState(false);
  const topRef = React.createRef();
  const tableRef = React.createRef();

  React.useEffect(() => {
    getData().then((data) =>
      setSpots({ normal: [...data], filtered: [...data] })
    );
  }, []);

  const closePopup = () => {
    setSelectedSpot({ ...selectedSpot, openPopup: false });
  };

  const filterSpots = (options) => {
    let filteredSpots = [];

    spots.normal.forEach((spot) => {
      const country = spot.country.toLowerCase();
      const wind = parseInt(spot.probability);
      if (
        country.includes(options.country.toLowerCase()) &&
        wind > parseInt(options.wind)
      ) {
        filteredSpots.push(spot);
      }
    });
    setSpots({ ...spots, filtered: filteredSpots });
  };

  const resetSpots = () => setSpots({ ...spots, filtered: [...spots.normal] });

  const addToFavourites = (spot, index) => {
    //api operations
    axios
      .post("/favourites", { spot: spot.id })
      .then((response) => {
        //local operations
        setSpots((prevSpots) => {
          prevSpots.normal[index].favourite = true;
          prevSpots.normal[index].favID = response.data.id;
          prevSpots.filtered[index].favourite = true;
          prevSpots.filtered[index].favID = response.data.id;

          return {
            ...prevSpots,
            normal: [...prevSpots.normal],
            filtered: [...prevSpots.filtered],
          };
        });
      })
      .catch(() => setError(true));
  };

  const removeFromFavourites = (spot, index) => {
    //api operations
    axios
      .delete("./favourites/" + spot.favID)
      .then((response) => {
        //local operations
        setSpots((prevSpots) => {
          prevSpots.normal[index].favourite = false;
          prevSpots.filtered[index].favourite = false;

          return {
            ...prevSpots,
            normal: [...prevSpots.normal],
            filtered: [...prevSpots.filtered],
          };
        });
      })
      .catch(() => setError(true));
  };

  return (
    <div style={{ height: "100%" }}>
      {error && <ErrorCard show={error} setShow={setError} />}

      <AppBar setOpenNewSpotModal={setOpenNewSpotModal} ref={topRef} />
      {openNewSpotModal && (
        <NewSpotModal
          updateMap={() => {
            getData().then((data) =>
              setSpots({ normal: [...data], filtered: [...data] })
            );
          }}
          close={() => setOpenNewSpotModal(false)}
        />
      )}

      <FilterButton
        filter={filterSpots}
        reset={resetSpots}
        closePopup={closePopup}
      />

      <Map spots={spots.filtered} setSelectedSpot={setSelectedSpot} />

      {selectedSpot.openPopup && (
        <Popup
          spot={spots.filtered[selectedSpot.index]}
          index={selectedSpot.index}
          addToFavourites={addToFavourites}
          removeFromFavourites={removeFromFavourites}
          close={closePopup}
        />
      )}

      <ScrollButton ref={{ top: topRef, table: tableRef }} />
      
      <Table data={spots.normal} ref={tableRef} />

      <Footer/>
    </div>
  );
});

export default Dashboard;
