import React from "react";
import "./App.css";
import UserContext from "./contexts/UserContext";
import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";

function App() {
  const [user, setUser] = React.useState(null);

  return (
    <UserContext.Provider value={{ user, setUser }}>
      <div className="App">
        {user === null ? <Login /> : <Dashboard />}
      </div>
    </UserContext.Provider>
  );
}

export default App;
