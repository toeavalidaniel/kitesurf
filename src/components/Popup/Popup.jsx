import styles from "./Popup.module.css";

const infos = [
  {
    name: "WIND PROBABILITY",
    key: "probability",
  },
  {
    name: "LATITUDE",
    key: "lat",
  },
  {
    name: "LONGITUDE",
    key: "long",
  },
  {
    name: "WHEN TO GO",
    key: "month",
  },
];


const Popup = ({ spot, index,  addToFavourites, removeFromFavourites, close }) => {
  return (
    <div className={styles.card}>
      <div className={styles.top}>
        <div className={styles.left}>
          <span className={styles.title}>
            {spot.name || ""}
            {spot.favourite? '⭐' : null}
          </span>
          <span className={styles.subtitle}>{spot.country || ""}</span>
        </div>
        <span
          style={{cursor:'pointer'}}
          onClick={(e) => {
            e.preventDefault();
            close();
          }}
        >
          <img alt="" src="/assets/close-icon.svg" />
        </span>
      </div>
      <div className={styles.content}>
        {infos.map((info, index) => (
          <div className={styles.info} key={index}>
            <span className={styles.infoTitle}>{info.name}</span>
            <span>
              {spot[info.key]}
              {info.key=== 'lat' ? '° N' : info.key === 'long' ? '° W' : null}
            </span>
          </div>
        ))}
      </div>
      <div className={styles.actions}>
        {!spot.favourite ? (
          <button 
            className={styles.button + ' ' + styles.addButton}
            onClick={() => {
              addToFavourites(spot, index);
            }}
          >
            + ADD TO FAVORITES
          </button>
        ) : (
          <button
            className={styles.button + ' ' + styles.removeButton}
            onClick={() => {
              removeFromFavourites(spot, index);
            }}
          >
            - REMOVE FROM FAVORITES
          </button>
        )}
      </div>
    </div>
  );
};

export default Popup;
