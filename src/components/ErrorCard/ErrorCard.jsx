import React from "react";

const ErrorCard = ({ show, setShow }) => {
  React.useEffect(() => {
    const timer = setTimeout(() => {
      setShow(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [setShow]);

  return (
    <>
      {show ? (
        <div
          style={{
            position: "absolute",
            top: 75,
            left: 50,
            zIndex: 9999,
            background: "white",
            padding: 10,
            filter: "drop-shadow(0px 2px 4px rgba(0, 0, 0, 0.5))",
          }}
        >
          <span>Eroare la server. Incearca din nou.</span>
        </div>
      ) : (
        <></>
      )}
    </>
  );
};

export default ErrorCard;
