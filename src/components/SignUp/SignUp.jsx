import React from "react";
import axios from "axios";
import styles from "./SignUp.module.css";

const SignUp = ({ switchToSignIn }) => {
  const [account, setAccount] = React.useState({
    username: "",
    password: "",
    email: "",
  });

  const [succes, setSucces] = React.useState(null);

  React.useEffect(() => {
    const timeout = setTimeout(() => {
      setSucces(null);
      switchToSignIn();
    }, 2000);
    return () => clearTimeout(timeout);
  }, [succes, switchToSignIn]);

  const handleSubmit = (e) => {
    e.preventDefault();
    axios
      .post("/user", account)
      .then((response) => setSucces(true))
      .catch(() => setSucces(false));
  };

  const handleInput = (e) => {
    setAccount({ ...account, [e.target.name]: e.target.value });
  };

  return (
    <div className={styles.content}>
      <img className={styles.logo} alt="logo" src="/logo.svg" />
      {succes !== null && (
        <div style={{ padding: 5, fontSize: 16, color: "#007AFF" }}>
          {succes ? (
            <div>
              <p>Account Created.</p>{" "}
              <p>Redirecting to Sign In in 2 seconds!</p>
            </div>
          ) : (
            <p style={{color:'red'}}>Server Error! Cannot create an account"</p>
          )}
        </div>
      )}
      <form className={styles.form} onSubmit={handleSubmit}>
        <label className={styles.label} htmlFor="username">
          Username
        </label>
        <input
          type="text"
          className={styles.input}
          id="username"
          name="username"
          value={account.username}
          onChange={handleInput}
        />
        <label className={styles.label} htmlFor="email">
          Email
        </label>
        <input
          type="text"
          className={styles.input}
          id="email"
          name="email"
          value={account.email}
          onChange={handleInput}
        />
        <label className={styles.label} htmlFor="password">
          Password
        </label>
        <input
          type="password"
          className={styles.input}
          id="password"
          name="password"
          value={account.password}
          onChange={handleInput}
        />
        <button className={styles.button}>Sign up</button>
      </form>
      <a
        href="/"
        onClick={(e) => {
          e.preventDefault();
          switchToSignIn();
        }}
      >
        Already have an account? Sign in now!
      </a>
    </div>
  );
};

export default SignUp;
