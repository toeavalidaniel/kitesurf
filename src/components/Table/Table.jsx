import React from "react";
import styles from "./Table.module.css";

const initColumns = () => [
  { name: "Name", id: "name", isNumber: false, ascending: true },
  { name: "Country", id: "country", isNumber: false, ascending: true },
  { name: "Latitude", id: "lat", isNumber: true, ascending: true },
  { name: "Longitude", id: "long", isNumber: true, ascending: true },
  { name: "Wind Prob.", id: "probability", isNumber: true, ascending: true },
  { name: "When to go", id: "month", isNumber: false, ascending: true },
];

const Table = React.forwardRef(({ data }, ref) => {
  const [spots, setSpots] = React.useState([]);
  const [columns, setColumns] = React.useState(initColumns());

  React.useEffect(() => {
    setSpots(data);
  }, [data]);

  const searchByName = (text) => {
    let filteredSpots = [];

    data.forEach((spot) => {
      const name = spot.name.toLowerCase();
      if (name.includes(text.toLowerCase())) {
        filteredSpots.push(spot);
      }
    });
    setSpots(filteredSpots);
  };

  const sortSpots = (column) => {
    let sortedSpots = [...spots];

    if (column.ascending) {
      //sort ascending order
      if (!column.isNumber) {
        sortedSpots.sort((spot1, spot2) => {
          let val1 = spot1[column.id].toUpperCase();
          let val2 = spot2[column.id].toUpperCase();
          if (val1 < val2) {
            return -1;
          }
          if (val1 > val2) {
            return 1;
          }
          return 0;
        });
      } else {
        sortedSpots.sort((spot1, spot2) => {
          return parseFloat(spot1[column.id]) - parseFloat(spot2[column.id]);
        });
      }
    } else {
      //sort descending order
      if (!column.isNumber) {
        sortedSpots.sort((spot1, spot2) => {
          let val1 = spot1[column.id].toUpperCase();
          let val2 = spot2[column.id].toUpperCase();
          if (val1 > val2) {
            return -1;
          }
          if (val1 < val2) {
            return 1;
          }
          return 0;
        });
      } else {
        sortedSpots.sort((spot1, spot2) => {
          return parseFloat(spot2[column.id]) - parseFloat(spot1[column.id]);
        });
      }
    }

    setSpots(sortedSpots);

    let updatedColumns = [...columns];
    const col = updatedColumns.find((col) => col.name === column.name);
    col.ascending = !col.ascending; //change the ascending var

    setColumns(updatedColumns);
  };

  return (
    <div className={styles.container}>
      <table className={styles.table} id="table" ref={ref}>
        <caption className={styles.header}>
          <span className={styles.headerTitle}>Locations</span>
          <div className={styles.searchBar}>
            <img alt="" src="assets/search.svg" style={{ height: 25 }} />
            <input
              className={styles.searchInput}
              placeholder="Search by name"
              onChange={(e) => {
                searchByName(e.target.value);
              }}
            />
          </div>
        </caption>
        <thead>
          <tr className={styles.columnTitle}>
            {columns.map((column, index) => (
              <th key={index}>
                <div className={styles.columnName}>
                  <span>{column.name}</span>
                  <a
                    href="/"
                    className={styles.sortButton}
                    onClick={(e) => {
                      e.preventDefault();
                      sortSpots(column);
                    }}
                  >
                    <img
                      alt=""
                      src="assets/sort-icon.svg"
                      style={{ height: 18 }}
                    />
                  </a>
                </div>
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {spots.map((spot, index) => (
            <tr
              className=""
              key={index}
              style={{ background: index % 2 === 0 ? "white" : "#F3F3F3" }}
            >
              <td data-title="Name">
                {spot.favourite ? "⭐ " : null}
                {spot.name}
              </td>
              <td data-title="Country">{spot.country}</td>
              <td data-title="Latitude">{Math.round(spot.lat * 100) / 100}</td>
              <td data-title="Longitude">
                {Math.round(spot.long * 100) / 100}
              </td>
              <td data-title="Wind Prob.">{spot.probability + "%"}</td>
              <td data-title="When to go">{spot.month}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
});

export default Table;
