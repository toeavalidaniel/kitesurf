import React from "react";
import styles from "./ScrollButton.module.css";

const ScrollButton = React.forwardRef((props, ref) => {
  const [scrollTo, setScrollTo] = React.useState("TABLE");

  return (
    <div className={styles.card}>
      <span
        style={{cursor:'pointer'}}
        className={styles.button}
        onClick={(e) => {
          e.preventDefault();

          if (scrollTo === "TABLE") {
            ref.table.current.scrollIntoView({ behavior: "smooth" });
            setScrollTo("TOP");
          } else {
            ref.top.current.scrollIntoView({ behavior: "smooth" });
            setScrollTo("TABLE");
          }
        }}
      >
        SCROLL TO {scrollTo}
      </span>
    </div>
  );
});

export default ScrollButton;
