import React from 'react'


const Footer = () => {
  return(
    <div 
      style={{
        height:300, 
        width:'100%', 
        background:'#D8D8D8',
        display:'flex',
        justifyContent:'center',
        alignItems:'center'
      }}
    >
      <img alt="logo" src="/logo.svg"/>
    </div>
  );
}

export default Footer;