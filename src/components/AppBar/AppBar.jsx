import React from "react";
import styles from "./AppBar.module.css";
import UserContext from "../../contexts/UserContext";

const AppBar = React.forwardRef(({ setOpenNewSpotModal }, ref) => {
  const [openMenu, setOpenMenu] = React.useState(false);
  const { setUser } = React.useContext(UserContext);

  return (
    <div className={styles.appBar} id="top" ref={ref}>
      <div className={styles.innerAppBar}>
        <div className={styles.leftSide}>
          <a href="/#">
            <img className={styles.logo} alt="" src="/logo.svg" />
          </a>
        </div>
        <div className={styles.rightSide}>
          <button
            className={styles.button}
            onClick={() => setOpenNewSpotModal(true)}
          >
            Add Spot
          </button>
          <div className={styles.holder}>
            <button
              className={styles.iconButton}
              onClick={() => setOpenMenu(true)}
            >
              <img
                alt=""
                src="/assets/user-icon.svg"
                style={{ height: "2.2rem" }}
              />
            </button>
            {openMenu && (
              <>
                <div className={styles.userMenu}>
                  <button
                    className={styles.logoutButton}
                    onClick={() => {
                      setUser(null);
                      window.history.replaceState("", document.title, "/");
                    }}
                  >
                    <img alt="" src="assets/exit-icon.svg" />
                    Log out
                  </button>
                </div>
                <div
                  className={styles.overlay}
                  onClick={() => setOpenMenu(false)}
                ></div>
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
});

export default AppBar;
