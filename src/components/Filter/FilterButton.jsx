import React from "react";
import styles from "./Filter.module.css";
import FilterMenu from "./FilterMenu";

const FilterButton = ({ filter, reset, closePopup }) => {
  const [openMenu, setOpenMenu] = React.useState(false);

  return (
    <div className={styles.holder}>
      {!openMenu ? (
        <button className={styles.button} onClick={() => setOpenMenu(true)}>
          <img alt="" src="assets/filter-icon.svg" />
          Filter
        </button>
      ) : (
        <FilterMenu
          setOpenMenu={setOpenMenu}
          filter={filter}
          reset={reset}
          closePopup={closePopup}
        />
      )}
    </div>
  );
};

export default FilterButton;
