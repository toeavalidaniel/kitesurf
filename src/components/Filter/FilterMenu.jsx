import React from "react";
import styles from "./Filter.module.css";

const FilterMenu = ({ setOpenMenu, filter, reset, closePopup }) => {
  const [options, setOptions] = React.useState({ country: "", wind: 0 });

  return (
    <div className={styles.card}>
      <div className={styles.top}>
        <span
          style={{cursor:'pointer'}}
          onClick={(e) => {
            e.preventDefault();
            setOpenMenu(false);
          }}
        >
          <img alt="close" src="/assets/close-icon.svg" />
        </span>
      </div>
      <div className={styles.content}>
        <label className={styles.label}>Country</label>
        <input
          className={styles.input}
          type="text"
          value={options.country}
          onChange={(e) => setOptions({ ...options, country: e.target.value })}
        />
        <label className={styles.label}>Wind Probability</label>
        <input
          className={styles.input}
          type="number"
          value={options.wind}
          onChange={(e) => setOptions({ ...options, wind: e.target.value })}
        />
        <div className={styles.buttonGroup}>
          <button
            className={styles.button + " " + styles.applyButton}
            onClick={(e) => {
              e.preventDefault();

              if (options.wind === "") {
                const newOptions = { ...options, wind: 0 };
                setOptions(newOptions);
                filter(newOptions);
              } else filter(options);

              closePopup();
            }}
          >
            APPLY FILTER
          </button>
          <button
            className={styles.button + " " + styles.resetButton}
            onClick={() => {
              setOptions({ country: "", wind: 0 });
              reset();
              closePopup();
            }}
          >
            Reset
          </button>
        </div>
      </div>
    </div>
  );
};

export default FilterMenu;
