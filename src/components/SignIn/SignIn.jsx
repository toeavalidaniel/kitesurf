import React from "react";
import axios from "axios";
import styles from "./SignIn.module.css";
import UserContext from "../../contexts/UserContext";

const SignIn = ({ switchToSignUp }) => {
  const [account, setAccount] = React.useState({
    username: "",
    password: "",
  });

  const { setUser } = React.useContext(UserContext);

  const handleSubmit = (e) => {
    e.preventDefault();

    axios
      .post("/login", {})
      .then((response) => setUser(response.data))
      .catch(() => {
        alert("Database is full. Proceeding without an account!");
        setUser({});
      });
  };

  const handleInput = (e) => {
    setAccount({ ...account, [e.target.name]: e.target.value });
  };

  return (
    <div className={styles.content}>
      <img className={styles.logo} alt="logo" src="/logo.svg" />
      <form className={styles.form} onSubmit={handleSubmit}>
        <label className={styles.label} htmlFor="username">
          Username
        </label>
        <input
          type="text"
          className={styles.input}
          id="username"
          name="username"
          value={account.username}
          onChange={handleInput}
        />
        <label className={styles.label} htmlFor="password">
          Password
        </label>
        <input
          type="password"
          className={styles.input}
          id="password"
          name="password"
          value={account.password}
          onChange={handleInput}
        />
        <button className={styles.button}>Login</button>
      </form>
      <a
        href="/"
        onClick={(e) => {
          e.preventDefault();
          switchToSignUp();
        }}
      >
        Don't have an account? Sign up now.
      </a>
    </div>
  );
};

export default SignIn;
