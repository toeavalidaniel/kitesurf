import React from "react";
import styles from "./NewSpotModal.module.css";
import DateFnsUtils from "@date-io/date-fns";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import axios from "axios";

import MiniMap from "../Map/MiniMap";

const addSpot = (data) => {
  return axios.post("/spot", data).then((response) => response.data);
};

const NewSpotModal = ({ updateMap, close }) => {
  const [data, setData] = React.useState(() =>{
    const newDate = new Date();
    return{
      name: "",
      country: "",
      date: newDate,
      month: newDate.toLocaleString("default", { month: "long" }),
      long: 0,
      lat: 0,
    }
  });

  const setLatLong = (lat, long) => {
    setData({ ...data, lat: lat, long: long });
  };

  return (
    <div className={styles.overlay}>
      <div className={styles.card}>
        <div className={styles.top}>
          <div className={styles.title}>Add Spot</div>
        </div>
        <div className={styles.body}>
          <div className={styles.form}>
            <label className={styles.label}>Name</label>
            <input
              className={styles.input}
              type="text"
              value={data.name}
              onChange={(e) => setData({ ...data, name: e.target.value })}
            />
            <label className={styles.label}>Country</label>
            <input
              className={styles.input}
              type="text"
              value={data.country}
              onChange={(e) => setData({ ...data, country: e.target.value })}
            />
            <label className={styles.label}>High Season</label>
            <MuiPickersUtilsProvider utils={DateFnsUtils} >
              <DatePicker
                style={{marginBottom:15}}
                value={data.date}
                onChange={(newDate) =>
                  setData({
                    ...data,
                    date: newDate,
                    month: newDate.toLocaleString("default", { month: "long" }),
                  })
                }
              />
            </MuiPickersUtilsProvider>
            <div className={styles.mapContainer}>
              <MiniMap setLatLong={setLatLong} />
            </div>
          </div>
          <div className={styles.buttonGroup}>
            <button
              className={styles.button}
              style={{ color: "#FF3B30" }}
              onClick={close}
            >
              CANCEL
            </button>
            <button
              className={styles.button}
              style={{ color: "#007AFF" }}
              onClick={() => {
                addSpot(data).then(() => updateMap());
                close();
              }}
            >
              CONFIRM
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewSpotModal;
