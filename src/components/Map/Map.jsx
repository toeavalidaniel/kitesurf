import React from "react";
import { MapContainer, TileLayer, Marker } from "react-leaflet";
import Leaflet from "leaflet";

const mapBounds = () => {
  const corner1 = Leaflet.latLng(-90, -200);
  const corner2 = Leaflet.latLng(90, 200);
  const bounds = Leaflet.latLngBounds(corner1, corner2);
  return bounds;
};

const markerIcon = Leaflet.icon({
  iconUrl: "/assets/marker.svg",

  iconSize: [38, 95],
  iconAnchor: [22, 94],
  popupAnchor: [-3, -76],
});

const favouriteIcon = Leaflet.icon({
  iconUrl: "/assets/favorite-marker.svg",

  iconSize: [38, 95],
  iconAnchor: [22, 94],
  popupAnchor: [-3, -76],
});

const Map = React.memo(({ spots, setSelectedSpot }) => {
  const position = [48.864716, 2.349]; // Paris position

  return (
    <div>
      <MapContainer
        center={position}
        zoom={3}
        scrollWheelZoom={true}
        maxBoundsViscosity={1.0}
        maxBounds={mapBounds()}
        style={{height:'calc(100vh - 52px)'}}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {spots && 
          spots.map((spot, index) => {
            return (
            <Marker
              key={index}
              position={[spot.lat, spot.long]}
              icon={spot.favourite ? favouriteIcon :  markerIcon}
              eventHandlers={{
                click: (e) => {
                  setSelectedSpot({ index: index, openPopup: true });
                },
              }}
            ></Marker>
          )})}
      </MapContainer>
    </div>
  );
});

export default Map;
