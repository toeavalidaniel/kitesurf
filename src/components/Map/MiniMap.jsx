import React from "react";
import { MapContainer, TileLayer, useMapEvents } from "react-leaflet";
import Leaflet from "leaflet";

const mapBounds = () => {
  const corner1 = Leaflet.latLng(-90, -200);
  const corner2 = Leaflet.latLng(90, 200);
  const bounds = Leaflet.latLngBounds(corner1, corner2);
  return bounds;
};

const MiniMap = React.memo(({ setLatLong }) => {
  const position = [48.864716, 2.349]; // Paris position
  //const [center, setCenter] = React.useState(position);

  return (
    <div>
      <MapContainer
        center={position}
        zoom={2}
        scrollWheelZoom={true}
        maxBoundsViscosity={1.0}
        maxBounds={mapBounds()}
        style={{ height: 220 }}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <MapEvents setCenter={setLatLong} />
      </MapContainer>
    </div>
  );
});

const MapEvents = ({ setCenter }) => {
  useMapEvents({
    dragend: (e) => {
      const center = e.target.getCenter();
      setCenter(center.lat, center.lng);
    },
    zoomend: (e) => {
      const center = e.target.getCenter();
      setCenter(center.lat, center.lng);
    },
  });
  return null;
};

export default MiniMap;
