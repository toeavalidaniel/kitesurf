import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import './index.css';
import App from './App';
import 'typeface-roboto';

axios.defaults.baseURL = "https://605d3f619386d200171ba601.mockapi.io";

ReactDOM.render(
    <App />,
  document.getElementById('root')
);